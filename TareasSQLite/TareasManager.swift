//
//  TareasManager.swift
//  TareasSQLite
//
//  Created by Fran on 8/1/18.
//  Copyright © 2018 Fran. All rights reserved.
//

import Foundation
class TareasManager: DBManager {

    func listarTareas()->[Tarea] {
        let querySQL = "SELECT * FROM tareas"
        var statement : OpaquePointer?;
        var lista : [Tarea] = [];
        
        let result = sqlite3_prepare_v2(db, querySQL, -1, &statement, nil)
        if (result==SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                let id = sqlite3_column_int(statement, 0)
                let titulo = String(cString: sqlite3_column_text(statement, 1))
                let prioridad = Int(sqlite3_column_int(statement, 2))
                let unix_time = sqlite3_column_int(statement, 3);
                //TimeInterval es equivalente a Double. Hacemos una conversión
                let vencimiento = Date(timeIntervalSince1970: TimeInterval(unix_time))
                
                let tarea = Tarea(id: Int(id), titulo: titulo, prioridad:prioridad, vencimiento:vencimiento)
                
                lista.append(tarea)
            }
        }
        sqlite3_finalize(statement);
        /*for p in lista {
            print("\(p.id) \(p.titulo) \(p.prioridad) \(p.vencimiento)")
        }*/
        return lista
    }
}
