//
//  DBManager.swift
//  TareasSQLite
//
//  Created by Fran on 8/1/18.
//  Copyright © 2018 Fran. All rights reserved.
//

import Foundation
class DBManager {
    var db : OpaquePointer? = nil
    
    init(db nombreDB : String, reload: Bool) {
        if let dbCopiaURL = copyDB(conNombre: nombreDB, reload: reload) {
            if sqlite3_open(dbCopiaURL.path, &(self.db)) == SQLITE_OK {
                print("Base de datos \(dbCopiaURL) abierta OK")
            }
            else {
                let error = String(cString:sqlite3_errmsg(db))
                print("Error al intentar abrir la BD: \(error) ")
            }
        }
        else {
            print("El archivo no se encuentra")
        }
    }
    
    deinit {
        sqlite3_close(self.db)
    }
    
    //Copia la base de datos desde el bundle al directorio Documents, para que se pueda modificar
    //si el parámetro "machaca" es true, copia la BD aunque ya esté en Documents.
    //En una app normal esto no lo haríamos cada vez que arranquemos, ya que se machacaría la BD
    func copyDB(conNombre nombre : String, reload machaca : Bool)->URL? {
        let fileManager = FileManager.default
        let docsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let dbCopiaURL = docsURL.appendingPathComponent(nombre)
        let existe = fileManager.fileExists(atPath: dbCopiaURL.path)
        if  existe && !machaca {
            return dbCopiaURL
        }
        else {
            if let dbOriginalURL = Bundle.main.url(forResource: nombre, withExtension: "") {
                if (existe) {
                    try! fileManager.removeItem(at: dbCopiaURL)
                }
                if (try? fileManager.copyItem(at: dbOriginalURL, to: dbCopiaURL)) != nil {
                    return dbCopiaURL
                }
                else {
                    return nil
                }
            }
            else {
                return nil
            }
        }
    }
}

