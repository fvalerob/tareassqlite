//
//  Tarea.swift
//  TareasSQLite
//
//  Created by Fran on 8/1/18.
//  Copyright © 2018 Fran. All rights reserved.
//

import Foundation
struct Tarea{
    var id: Int
    var titulo: String
    var prioridad: Int
    var vencimiento: Date
}


